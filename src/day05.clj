(ns day05
  (:require [clojure.string :as str]))

(defn line-to-crates [line]
  (let [stack-count (/ (inc (count line)) 4)
        pos (range 1 (* stack-count 4) 4)
        crate-chars (map #(get line %) pos)
        crates (map #(if (= \space %) nil %) crate-chars)]
    crates))

(defn line-to-moves [line]
  (->> (re-seq #"move (\d+) from (\d+) to (\d+)" line)
       first
       (drop 1)
       (map #(Integer/parseInt %))))

(defn purge-nils [coll]
  (filter some? coll))

(defn transpose [list-of-lists]
  (let [empty-vector-of-lists (repeat (count (first list-of-lists)) (list))
        reversed-list-of-lists (reverse list-of-lists)]
    (reduce #(map conj %1 %2) empty-vector-of-lists reversed-list-of-lists)))

(defn rows-to-stacks [top-bottom-crate-rows]
  (map purge-nils (transpose top-bottom-crate-rows)))

(defn move [stacks move transport-f]
  (let [[how-many from to] move
        grab (take how-many (get stacks (dec from)))
        removed (update stacks (dec from) #(drop how-many %))
        crates (transport-f grab)
        added (update removed (dec to) #(concat crates %))]
    added))

(defn run-cratemover [stacks moves transport-f]
  (if (empty? moves) stacks
                     (recur (move stacks (first moves) transport-f) (rest moves) transport-f)))

(defn day05 [input transport-f]
  (let [[crate-lines move-lines] (str/split input #"\n\n")
        crate-rows (map line-to-crates (drop-last (str/split crate-lines #"\n")))
        stacks (apply vector (rows-to-stacks crate-rows))
        moves (map line-to-moves (str/split move-lines #"\n"))]
    (apply str (map first (run-cratemover stacks moves transport-f)))))

(defn day05-part1 [input]
  (day05 input reverse))

(defn day05-part2 [input]
  (day05 input identity))


(def example-input "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2")
(def puzzle-input "[S]                 [T] [Q]        \n[L]             [B] [M] [P]     [T]\n[F]     [S]     [Z] [N] [S]     [R]\n[Z] [R] [N]     [R] [D] [F]     [V]\n[D] [Z] [H] [J] [W] [G] [W]     [G]\n[B] [M] [C] [F] [H] [Z] [N] [R] [L]\n[R] [B] [L] [C] [G] [J] [L] [Z] [C]\n[H] [T] [Z] [S] [P] [V] [G] [M] [M]\n 1   2   3   4   5   6   7   8   9 \n\nmove 6 from 1 to 7\nmove 2 from 2 to 4\nmove 2 from 7 to 4\nmove 6 from 4 to 3\nmove 1 from 5 to 1\nmove 3 from 8 to 3\nmove 15 from 3 to 4\nmove 6 from 5 to 9\nmove 14 from 4 to 2\nmove 3 from 2 to 7\nmove 1 from 2 to 7\nmove 9 from 9 to 1\nmove 3 from 2 to 1\nmove 7 from 6 to 7\nmove 1 from 6 to 8\nmove 2 from 9 to 1\nmove 9 from 2 to 3\nmove 8 from 3 to 9\nmove 1 from 1 to 4\nmove 1 from 8 to 6\nmove 1 from 6 to 2\nmove 5 from 9 to 8\nmove 2 from 9 to 1\nmove 1 from 4 to 2\nmove 17 from 1 to 9\nmove 1 from 3 to 1\nmove 3 from 2 to 3\nmove 2 from 4 to 5\nmove 12 from 7 to 3\nmove 16 from 9 to 2\nmove 5 from 7 to 5\nmove 2 from 1 to 2\nmove 1 from 3 to 6\nmove 1 from 4 to 6\nmove 1 from 7 to 3\nmove 1 from 6 to 3\nmove 7 from 3 to 4\nmove 5 from 8 to 3\nmove 1 from 6 to 7\nmove 7 from 3 to 4\nmove 6 from 3 to 1\nmove 2 from 4 to 8\nmove 1 from 5 to 2\nmove 10 from 4 to 5\nmove 3 from 5 to 2\nmove 2 from 8 to 9\nmove 5 from 2 to 8\nmove 1 from 3 to 5\nmove 2 from 5 to 8\nmove 12 from 5 to 7\nmove 1 from 4 to 2\nmove 5 from 9 to 4\nmove 1 from 2 to 5\nmove 6 from 1 to 3\nmove 6 from 3 to 5\nmove 10 from 7 to 4\nmove 2 from 7 to 3\nmove 4 from 7 to 6\nmove 1 from 9 to 5\nmove 12 from 2 to 1\nmove 1 from 8 to 7\nmove 3 from 7 to 4\nmove 4 from 4 to 8\nmove 7 from 5 to 3\nmove 1 from 2 to 4\nmove 10 from 1 to 5\nmove 2 from 1 to 2\nmove 4 from 6 to 7\nmove 8 from 8 to 3\nmove 5 from 4 to 9\nmove 12 from 3 to 8\nmove 4 from 3 to 8\nmove 2 from 9 to 2\nmove 3 from 5 to 4\nmove 1 from 3 to 5\nmove 1 from 7 to 6\nmove 14 from 4 to 6\nmove 6 from 5 to 9\nmove 8 from 2 to 8\nmove 3 from 5 to 7\nmove 21 from 8 to 4\nmove 16 from 4 to 9\nmove 8 from 6 to 2\nmove 4 from 6 to 1\nmove 1 from 4 to 6\nmove 2 from 4 to 8\nmove 3 from 1 to 8\nmove 2 from 4 to 6\nmove 1 from 6 to 2\nmove 3 from 8 to 4\nmove 2 from 2 to 5\nmove 2 from 5 to 7\nmove 1 from 8 to 9\nmove 1 from 4 to 9\nmove 1 from 1 to 6\nmove 3 from 6 to 3\nmove 3 from 2 to 3\nmove 1 from 4 to 6\nmove 3 from 6 to 7\nmove 10 from 9 to 7\nmove 1 from 4 to 7\nmove 6 from 8 to 3\nmove 1 from 6 to 8\nmove 2 from 2 to 5\nmove 1 from 2 to 1\nmove 1 from 8 to 9\nmove 1 from 2 to 8\nmove 1 from 1 to 9\nmove 7 from 9 to 1\nmove 1 from 8 to 5\nmove 7 from 1 to 7\nmove 3 from 5 to 8\nmove 3 from 7 to 2\nmove 1 from 8 to 4\nmove 1 from 2 to 4\nmove 2 from 4 to 6\nmove 5 from 3 to 1\nmove 9 from 7 to 2\nmove 6 from 3 to 8\nmove 8 from 2 to 7\nmove 2 from 6 to 4\nmove 2 from 1 to 7\nmove 2 from 1 to 4\nmove 24 from 7 to 4\nmove 4 from 8 to 9\nmove 2 from 7 to 5\nmove 1 from 5 to 2\nmove 1 from 3 to 8\nmove 4 from 2 to 8\nmove 13 from 9 to 2\nmove 2 from 8 to 6\nmove 3 from 9 to 6\nmove 26 from 4 to 2\nmove 1 from 5 to 7\nmove 2 from 6 to 2\nmove 2 from 4 to 1\nmove 7 from 2 to 1\nmove 15 from 2 to 6\nmove 8 from 2 to 8\nmove 4 from 6 to 8\nmove 9 from 2 to 9\nmove 13 from 6 to 7\nmove 6 from 1 to 9\nmove 2 from 2 to 4\nmove 4 from 1 to 6\nmove 3 from 8 to 3\nmove 1 from 4 to 9\nmove 2 from 6 to 7\nmove 1 from 4 to 3\nmove 3 from 3 to 2\nmove 14 from 7 to 4\nmove 5 from 9 to 5\nmove 9 from 8 to 5\nmove 7 from 9 to 6\nmove 2 from 5 to 6\nmove 2 from 9 to 2\nmove 10 from 5 to 1\nmove 1 from 3 to 1\nmove 2 from 8 to 1\nmove 1 from 9 to 2\nmove 1 from 7 to 5\nmove 4 from 2 to 1\nmove 1 from 9 to 8\nmove 3 from 4 to 1\nmove 1 from 8 to 6\nmove 12 from 1 to 5\nmove 1 from 1 to 6\nmove 1 from 7 to 5\nmove 4 from 6 to 9\nmove 2 from 2 to 4\nmove 1 from 9 to 6\nmove 1 from 1 to 5\nmove 2 from 9 to 7\nmove 10 from 6 to 5\nmove 1 from 6 to 7\nmove 20 from 5 to 1\nmove 1 from 7 to 9\nmove 2 from 9 to 1\nmove 3 from 5 to 1\nmove 2 from 8 to 4\nmove 2 from 8 to 7\nmove 1 from 5 to 9\nmove 1 from 8 to 4\nmove 22 from 1 to 7\nmove 5 from 4 to 8\nmove 1 from 5 to 9\nmove 19 from 7 to 4\nmove 2 from 9 to 1\nmove 1 from 5 to 9\nmove 10 from 1 to 8\nmove 1 from 9 to 1\nmove 1 from 8 to 3\nmove 8 from 4 to 7\nmove 1 from 5 to 6\nmove 3 from 4 to 5\nmove 1 from 5 to 9\nmove 11 from 7 to 4\nmove 4 from 4 to 9\nmove 1 from 6 to 2\nmove 1 from 3 to 9\nmove 5 from 9 to 4\nmove 5 from 7 to 9\nmove 23 from 4 to 2\nmove 17 from 2 to 7\nmove 2 from 2 to 8\nmove 4 from 4 to 7\nmove 1 from 4 to 5\nmove 2 from 5 to 2\nmove 5 from 8 to 9\nmove 5 from 2 to 7\nmove 9 from 7 to 5\nmove 11 from 9 to 2\nmove 1 from 4 to 3\nmove 5 from 8 to 7\nmove 3 from 8 to 5\nmove 2 from 1 to 3\nmove 2 from 3 to 9\nmove 1 from 5 to 8\nmove 5 from 7 to 5\nmove 15 from 5 to 4\nmove 2 from 8 to 1\nmove 2 from 5 to 1\nmove 4 from 4 to 1\nmove 1 from 8 to 7\nmove 8 from 2 to 1\nmove 4 from 2 to 8\nmove 2 from 7 to 4\nmove 5 from 8 to 6\nmove 5 from 7 to 9\nmove 4 from 6 to 5\nmove 7 from 4 to 8\nmove 1 from 6 to 1\nmove 1 from 3 to 1\nmove 2 from 5 to 1\nmove 7 from 1 to 5\nmove 5 from 1 to 3\nmove 4 from 7 to 9\nmove 4 from 3 to 9\nmove 2 from 9 to 7\nmove 6 from 9 to 2\nmove 1 from 4 to 1\nmove 1 from 3 to 5\nmove 1 from 2 to 5\nmove 5 from 9 to 4\nmove 4 from 4 to 6\nmove 1 from 8 to 9\nmove 8 from 4 to 3\nmove 7 from 7 to 3\nmove 5 from 1 to 3\nmove 11 from 5 to 9\nmove 1 from 7 to 6\nmove 2 from 3 to 5\nmove 1 from 3 to 1\nmove 3 from 6 to 2\nmove 2 from 5 to 1\nmove 2 from 1 to 2\nmove 3 from 1 to 5\nmove 5 from 9 to 2\nmove 2 from 6 to 8\nmove 2 from 3 to 8\nmove 4 from 9 to 7\nmove 3 from 5 to 2\nmove 2 from 1 to 8\nmove 1 from 9 to 8\nmove 1 from 9 to 2\nmove 4 from 7 to 9\nmove 11 from 8 to 7\nmove 1 from 8 to 2\nmove 6 from 9 to 7\nmove 3 from 7 to 1\nmove 13 from 2 to 7\nmove 24 from 7 to 1\nmove 2 from 2 to 6\nmove 1 from 8 to 3\nmove 1 from 9 to 3\nmove 5 from 2 to 4\nmove 1 from 2 to 5\nmove 1 from 6 to 2\nmove 1 from 6 to 3\nmove 1 from 2 to 4\nmove 3 from 7 to 3\nmove 2 from 1 to 7\nmove 2 from 3 to 8\nmove 2 from 7 to 8\nmove 9 from 3 to 2\nmove 3 from 4 to 8\nmove 1 from 5 to 1\nmove 9 from 2 to 1\nmove 3 from 4 to 9\nmove 1 from 7 to 8\nmove 6 from 3 to 9\nmove 2 from 1 to 5\nmove 15 from 1 to 3\nmove 13 from 3 to 9\nmove 11 from 1 to 4\nmove 5 from 4 to 1\nmove 6 from 3 to 6\nmove 4 from 4 to 8\nmove 6 from 1 to 4\nmove 1 from 5 to 2\nmove 1 from 2 to 1\nmove 3 from 4 to 2\nmove 2 from 8 to 5\nmove 2 from 4 to 2\nmove 9 from 9 to 3\nmove 9 from 3 to 5\nmove 2 from 9 to 4\nmove 5 from 2 to 6\nmove 1 from 1 to 8\nmove 1 from 4 to 1\nmove 10 from 9 to 2\nmove 9 from 2 to 4\nmove 10 from 4 to 1\nmove 3 from 1 to 3\nmove 4 from 1 to 2\nmove 5 from 2 to 4\nmove 2 from 5 to 2\nmove 4 from 1 to 7\nmove 10 from 5 to 4\nmove 2 from 2 to 4\nmove 1 from 9 to 2\nmove 2 from 3 to 5\nmove 1 from 3 to 5\nmove 3 from 6 to 7\nmove 8 from 4 to 9\nmove 6 from 6 to 1\nmove 4 from 9 to 5\nmove 2 from 9 to 1\nmove 1 from 2 to 6\nmove 6 from 5 to 2\nmove 3 from 7 to 9\nmove 4 from 8 to 2\nmove 1 from 7 to 9\nmove 1 from 5 to 3\nmove 2 from 7 to 4\nmove 1 from 7 to 1\nmove 14 from 1 to 9\nmove 1 from 1 to 9\nmove 1 from 3 to 8\nmove 3 from 2 to 5\nmove 2 from 4 to 2\nmove 6 from 8 to 1\nmove 1 from 2 to 1\nmove 5 from 1 to 9\nmove 1 from 1 to 7\nmove 2 from 8 to 5\nmove 1 from 5 to 4\nmove 1 from 6 to 1\nmove 8 from 2 to 7\nmove 2 from 6 to 1\nmove 9 from 9 to 5\nmove 11 from 4 to 8\nmove 4 from 7 to 4\nmove 6 from 4 to 6\nmove 1 from 7 to 4\nmove 6 from 6 to 7\nmove 1 from 5 to 9\nmove 6 from 8 to 9\nmove 8 from 9 to 5\nmove 1 from 4 to 5\nmove 15 from 9 to 3\nmove 3 from 1 to 4\nmove 6 from 7 to 2\nmove 3 from 4 to 9\nmove 2 from 7 to 3\nmove 1 from 7 to 3\nmove 1 from 7 to 2\nmove 2 from 8 to 1\nmove 3 from 8 to 5\nmove 2 from 1 to 7\nmove 8 from 3 to 6\nmove 3 from 6 to 5\nmove 1 from 6 to 1\nmove 10 from 5 to 7\nmove 6 from 5 to 4\nmove 4 from 2 to 4\nmove 6 from 5 to 1\nmove 6 from 1 to 8\nmove 2 from 9 to 2\nmove 2 from 9 to 7\nmove 6 from 3 to 7\nmove 1 from 3 to 5\nmove 1 from 1 to 9\nmove 2 from 8 to 1\nmove 2 from 5 to 4\nmove 3 from 3 to 7\nmove 10 from 4 to 6\nmove 1 from 9 to 7\nmove 12 from 7 to 3\nmove 12 from 3 to 8\nmove 2 from 1 to 5\nmove 1 from 1 to 3\nmove 13 from 8 to 1\nmove 7 from 7 to 1\nmove 13 from 6 to 9\nmove 1 from 7 to 4\nmove 6 from 5 to 3\nmove 3 from 4 to 3\nmove 6 from 3 to 1\nmove 10 from 9 to 4\nmove 2 from 7 to 6\nmove 8 from 1 to 9\nmove 3 from 2 to 9\nmove 1 from 3 to 5\nmove 1 from 3 to 5\nmove 1 from 1 to 4\nmove 6 from 9 to 3\nmove 2 from 6 to 7\nmove 4 from 9 to 5\nmove 4 from 1 to 6\nmove 1 from 2 to 4\nmove 6 from 1 to 4\nmove 3 from 9 to 3\nmove 3 from 6 to 8\nmove 3 from 8 to 7\nmove 5 from 5 to 1\nmove 1 from 3 to 9\nmove 1 from 9 to 5\nmove 1 from 3 to 2\nmove 2 from 5 to 1\nmove 1 from 6 to 9\nmove 1 from 6 to 3\nmove 2 from 9 to 7\nmove 2 from 8 to 1\nmove 1 from 3 to 2\nmove 1 from 2 to 5\nmove 1 from 7 to 1\nmove 7 from 7 to 9\nmove 12 from 1 to 9\nmove 1 from 5 to 2\nmove 1 from 7 to 1\nmove 13 from 4 to 7\nmove 1 from 9 to 4\nmove 5 from 7 to 3\nmove 4 from 9 to 1\nmove 8 from 7 to 9\nmove 3 from 2 to 3\nmove 4 from 3 to 7\nmove 5 from 4 to 6\nmove 3 from 9 to 4\nmove 10 from 1 to 5\nmove 3 from 4 to 7\nmove 16 from 9 to 2\nmove 3 from 9 to 2\nmove 6 from 5 to 3\nmove 4 from 6 to 2\nmove 1 from 4 to 6\nmove 2 from 6 to 8\nmove 1 from 5 to 2\nmove 1 from 5 to 8\nmove 7 from 7 to 2\nmove 16 from 2 to 1\nmove 1 from 5 to 1\nmove 10 from 2 to 8\nmove 14 from 8 to 5\nmove 2 from 2 to 6\nmove 1 from 2 to 5\nmove 2 from 2 to 1\nmove 8 from 1 to 7\nmove 4 from 1 to 7\nmove 2 from 1 to 7\nmove 5 from 3 to 2\nmove 1 from 1 to 6\nmove 2 from 2 to 5\nmove 4 from 1 to 7\nmove 1 from 2 to 8\nmove 1 from 2 to 8\nmove 3 from 6 to 7\nmove 10 from 7 to 5\nmove 1 from 2 to 8\nmove 27 from 5 to 9\nmove 1 from 5 to 6\nmove 1 from 6 to 4\nmove 1 from 4 to 3\nmove 3 from 3 to 7\nmove 4 from 3 to 6\nmove 2 from 6 to 4\nmove 3 from 8 to 1\nmove 2 from 6 to 1\nmove 12 from 7 to 8\nmove 2 from 3 to 9\nmove 1 from 9 to 2\nmove 1 from 2 to 8\nmove 2 from 1 to 2\nmove 6 from 3 to 8\nmove 1 from 7 to 4\nmove 15 from 9 to 5\nmove 7 from 9 to 4\nmove 1 from 2 to 1\nmove 16 from 8 to 2\nmove 8 from 5 to 2\nmove 24 from 2 to 9\nmove 3 from 1 to 2\nmove 24 from 9 to 1\nmove 5 from 5 to 9\nmove 3 from 4 to 1\nmove 1 from 7 to 6\nmove 1 from 6 to 3\nmove 1 from 3 to 2\nmove 3 from 2 to 3\nmove 1 from 5 to 6\nmove 1 from 2 to 7\n")


(comment

  [(= "CMZ" (day05-part1 example-input))
   (= "RNZLFZSJH" (day05-part1 puzzle-input))
   (= "MCD" (day05-part2 example-input))
   (= "CNSFCGJSM" (day05-part2 puzzle-input))])













