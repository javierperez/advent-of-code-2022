(ns day07
  (:require [clojure.string :as str]
            [clojure.test :refer :all]))

;workspace map
{:wd ["/"] :fs [{:dir "/" :file {:name "readme.txt" :size 10}}]}

(defn parse-command [command-string]
  (let [tokens (str/split command-string #" ")]
    (condp = (first tokens)
      "$" (condp = (second tokens)
            "cd" {:type :cd :arg (last tokens)}
            "ls" {:type :ls})
      "dir" {:type :dir :arg (second tokens)}
      {:type :file
       :name (second tokens)
       :size (Integer/parseInt (first tokens))})))

(defn update-path [path cd-path]
  (condp = cd-path
    "/" ["/"]
    ".." (into [] (drop-last path))
    (conj path cd-path)))

(defn update-wd [workspace cd-path]
  (update workspace :wd
          #(update-path % cd-path)))

(defn add-file [workspace file]
  (update workspace :fs
          #(conj % {:path (:wd workspace)
                    :file {:name (:name file)
                           :size (:size file)}})))

(defn use-command [workspace command]
  (condp = (:type command)
    :cd (update-wd workspace (:arg command))
    :file (add-file workspace command)
    workspace))

(defn build-listing [workspace commands]
  (if (empty? commands)
    workspace
    (recur (use-command workspace (first commands)) (rest commands))))

(defn build-fs [commands]
  (build-listing {:wd ["/"] :fs []} commands))

(defn size-contribution [file]
  (let [path (:path file)
        size (:size (:file file))
        dirs (map #(hash-map :dir (apply str (subvec path 0 %))
                             :size size)
                  (range 1 (inc (count path))))]
    dirs))

(defn size-contributions [files]
  (mapcat size-contribution files))

(defn directories-size
  ([size-contributions] (directories-size {} size-contributions))
  ([dir-sizes size-contributions] (if (empty? size-contributions)
                                    dir-sizes
                                    (recur (update dir-sizes (:dir (first size-contributions))
                                                   #(+ (if (nil? %) 0 %)
                                                       (:size (first size-contributions))))
                                           (rest size-contributions)))))


(defn day07 [dir-selection-f input]
  (->> (str/split input #"\n")
       (map parse-command)
       build-fs
       :fs
       size-contributions
       directories-size
       dir-selection-f))

(defn candidates-to-delete [dir-sizes]
  (->> dir-sizes
       vals
       (filter #(<= % 100000))
       (reduce +)))

(defn day07-part1 [input]
  (day07 candidates-to-delete input))

(defn best-to-delete [dir-sizes]
  (let [free (- 70000000 (get dir-sizes "/"))
        needed (- 30000000 free)]
    (->> dir-sizes
         vals
         (filter #(>= % needed))
         (sort -)
         first)))

(defn day07-part2 [input]
  (day07 best-to-delete input))

(def example-input "$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k")
(def puzzle-input "$ cd /\n$ ls\ndir gqcclj\ndir lmtpm\ndir nhqwt\ndir qcq\ndir vwqwlqrt\n$ cd gqcclj\n$ ls\n62425 dqp.gjm\n174181 hrtw.qsd\n273712 pflp.mdw\n169404 zlthnlhf.mtn\n180878 zprprf\n$ cd ..\n$ cd lmtpm\n$ ls\ndir clffsvcw\n163587 cvcl.jqh\ndir dcqnblb\ndir dtpwln\ndir fvt\ndir hrcrw\ndir jdqzmqn\n236754 nrdmlj\n205959 pflp.mdw\ndir qcq\ndir rsn\n129926 vdgcqdn.sqd\ndir zprprf\n$ cd clffsvcw\n$ ls\n6997 dcqnblb.wbh\n145711 dqp\n159225 pflp.mdw\n$ cd ..\n$ cd dcqnblb\n$ ls\ndir dcqnblb\ndir gfn\ndir lpswsp\ndir lvt\ndir zprprf\n$ cd dcqnblb\n$ ls\n2020 grpdmd.ggz\ndir zpswzfvg\n$ cd zpswzfvg\n$ ls\n206998 zprprf.gnw\n$ cd ..\n$ cd ..\n$ cd gfn\n$ ls\n277530 rhbvtblc.mvw\n$ cd ..\n$ cd lpswsp\n$ ls\n173180 dcqnblb\n$ cd ..\n$ cd lvt\n$ ls\ndir hjllwsvl\ndir ptbt\n$ cd hjllwsvl\n$ ls\ndir wqnc\n$ cd wqnc\n$ ls\n64695 grpdmd.ggz\n$ cd ..\n$ cd ..\n$ cd ptbt\n$ ls\n150880 vvbt.gtp\n$ cd ..\n$ cd ..\n$ cd zprprf\n$ ls\ndir ldzslndn\ndir qftt\n$ cd ldzslndn\n$ ls\ndir bwqqsbhg\n129454 vbn\n$ cd bwqqsbhg\n$ ls\n108701 zprprf.gss\n$ cd ..\n$ cd ..\n$ cd qftt\n$ ls\n64268 cvcl.jqh\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dtpwln\n$ ls\n196215 cvcl.jqh\ndir dpwg\ndir ldzslndn\ndir znnsqqh\n$ cd dpwg\n$ ls\n192388 gmh\n47754 grgzh.qdl\n99449 hqsh\ndir pbmf\n50061 pflp.mdw\n192902 qcq.pgg\ndir rmpvj\ndir scgc\n$ cd pbmf\n$ ls\n210083 wpfnwbl.mgf\n$ cd ..\n$ cd rmpvj\n$ ls\n125738 nmlnbvrd\n226214 zprprf.jnp\n114257 zprprf.srs\n$ cd ..\n$ cd scgc\n$ ls\n182115 rrc.rcc\n$ cd ..\n$ cd ..\n$ cd ldzslndn\n$ ls\n201992 qcrm.cpd\n$ cd ..\n$ cd znnsqqh\n$ ls\n85635 cvcl.jqh\n$ cd ..\n$ cd ..\n$ cd fvt\n$ ls\ndir dcqnblb\ndir gnc\n75864 vfn\n$ cd dcqnblb\n$ ls\ndir dcqnblb\ndir lbnflwsh\n$ cd dcqnblb\n$ ls\n269901 cvcl.jqh\n$ cd ..\n$ cd lbnflwsh\n$ ls\n33336 grpdmd.ggz\n42861 phg.wmc\n$ cd ..\n$ cd ..\n$ cd gnc\n$ ls\ndir jhjbjsp\ndir jjppr\n$ cd jhjbjsp\n$ ls\n96177 ldzslndn\n$ cd ..\n$ cd jjppr\n$ ls\n181016 dqp\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd hrcrw\n$ ls\n261376 dtjfpppr.dww\n54658 vsrgvw.pfn\n$ cd ..\n$ cd jdqzmqn\n$ ls\n52342 dcpndc.vlg\n171946 gggpchh.tbb\ndir ldzslndn\n11156 nbfrfvv.gzw\n$ cd ldzslndn\n$ ls\n107873 cvcl.jqh\n216034 gfdjrbz\n68844 pqllfrrh.jcf\n$ cd ..\n$ cd ..\n$ cd qcq\n$ ls\n152886 ldzslndn.ltn\n105125 vwplh.vbf\n$ cd ..\n$ cd rsn\n$ ls\n15385 hqcmjdgv.jjv\n105735 qcq.bzg\n58805 snczcsp\n26668 vbn\n$ cd ..\n$ cd zprprf\n$ ls\ndir chbmq\ndir dcqnblb\ndir dqp\ndir nfspb\n89506 zprprf.hnt\n$ cd chbmq\n$ ls\ndir cnjvw\ndir dqp\n151434 frsvrdnt\ndir msztjvcb\n240689 qcq.jlh\ndir sjzrcg\n97312 vnr.zfr\ndir zprprf\n$ cd cnjvw\n$ ls\ndir bpbs\n252403 cqhtshc\ndir djmjhn\n10935 fhqmswr\n6582 pdwml.ldd\ndir qcq\n219282 rfmd\n$ cd bpbs\n$ ls\n147582 bnhwsnsj.gdm\n61362 cvcl.jqh\n152857 vdgcqdn.sqd\n$ cd ..\n$ cd djmjhn\n$ ls\ndir bjdbcjbb\ndir dcqnblb\ndir dqp\ndir lgdwtt\n$ cd bjdbcjbb\n$ ls\n110710 cvcl.jqh\n252792 hmshctr.lgz\ndir mjhtmbj\n189745 shsswcgr\ndir tfnhp\n194940 vbn\ndir zprprf\n$ cd mjhtmbj\n$ ls\ndir dqp\ndir hbthpcmb\n$ cd dqp\n$ ls\n200832 sbcrz.qgw\n$ cd ..\n$ cd hbthpcmb\n$ ls\n55191 ffcntg\n$ cd ..\n$ cd ..\n$ cd tfnhp\n$ ls\n276825 dqp\n161538 gqmr.wgb\n$ cd ..\n$ cd zprprf\n$ ls\n287638 dcqnblb.ssp\n41274 hgmrvj.mwf\n249118 sbb.gsf\n105141 wwrg.gqz\n$ cd ..\n$ cd ..\n$ cd dcqnblb\n$ ls\n1957 btmmc\n32386 dtzbzg.dhm\ndir mmrbj\n98283 ntmhfgtl.pmf\ndir zprprf\n$ cd mmrbj\n$ ls\n273194 wnsq\n251527 zprprf\n$ cd ..\n$ cd zprprf\n$ ls\n27678 ldzslndn.rrl\n62866 ljf.fdj\n148502 qcq.dlg\ndir rvgqvm\n179231 tllnmhn.pjp\n64033 vbn\ndir zcdrj\n$ cd rvgqvm\n$ ls\ndir ntbv\n262324 prhgj.szz\ndir qbvdh\n$ cd ntbv\n$ ls\n116608 cgv.fvj\n175200 swpswq.twt\n$ cd ..\n$ cd qbvdh\n$ ls\n160353 sdhfrb.wjn\n$ cd ..\n$ cd ..\n$ cd zcdrj\n$ ls\n283262 ctl\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dqp\n$ ls\ndir jfzm\n111438 rdrgb.mjf\n64194 wgtmqrq\ndir zprprf\n$ cd jfzm\n$ ls\n158774 pflp.mdw\n$ cd ..\n$ cd zprprf\n$ ls\n215264 sgsstcp\n$ cd ..\n$ cd ..\n$ cd lgdwtt\n$ ls\ndir qcq\n$ cd qcq\n$ ls\n165461 ldzslndn.vvb\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd qcq\n$ ls\ndir dpd\n165044 grpdmd.ggz\n82343 ldzslndn\ndir mwg\n176689 psjcwp.wct\n44404 qcq.zwd\n$ cd dpd\n$ ls\n84087 dqp\n227386 zprprf.gfs\n$ cd ..\n$ cd mwg\n$ ls\n214086 pflp.mdw\ndir sjjsdn\n225859 wcdt\n158892 zprprf.frs\n$ cd sjjsdn\n$ ls\n260121 gplgp.dfn\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dqp\n$ ls\ndir hcrwclpg\ndir zphd\n$ cd hcrwclpg\n$ ls\ndir cmqntjj\n16393 ldzslndn.qbm\n91152 qqdtc.zdq\n$ cd cmqntjj\n$ ls\n272266 ldzslndn.pll\n$ cd ..\n$ cd ..\n$ cd zphd\n$ ls\n165711 chftwcsw.fqw\n256871 cvcl.jqh\n251168 zprprf.gfv\n$ cd ..\n$ cd ..\n$ cd msztjvcb\n$ ls\n206231 brzn.lmn\ndir dcqnblb\n21571 dqp\ndir fmn\n45779 mlfctz.cjr\n288827 pflp.mdw\n220578 qcq.fqf\n$ cd dcqnblb\n$ ls\n198121 ghbwgs\n93681 nmqhl.vpq\n$ cd ..\n$ cd fmn\n$ ls\n29407 mdfws.qvs\n$ cd ..\n$ cd ..\n$ cd sjzrcg\n$ ls\n155120 ddclvsjr.rpq\n136029 ldzslndn.dcm\ndir vhzh\n$ cd vhzh\n$ ls\n212446 vbn\n$ cd ..\n$ cd ..\n$ cd zprprf\n$ ls\n240335 crt.gqh\n185363 gnmm.qgh\ndir ldzslndn\ndir nwl\ndir qll\n277043 vbn\n217796 vtvgpdl.vtm\n$ cd ldzslndn\n$ ls\n273570 cvcl.jqh\n68510 fgdmz.hrc\ndir npq\ndir swjrzzrm\n$ cd npq\n$ ls\n97923 dzcjsqwt\n$ cd ..\n$ cd swjrzzrm\n$ ls\n180599 tmpgn.bjf\n$ cd ..\n$ cd ..\n$ cd nwl\n$ ls\n171833 dlwrfhh.qgn\n$ cd ..\n$ cd qll\n$ ls\n219926 dcqnblb.bvn\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dcqnblb\n$ ls\ndir lvpb\n276198 tbgcm.qct\n$ cd lvpb\n$ ls\n142590 bvhjlld\n268259 gnjfg.sgb\ndir qcq\n206220 qcq.zsg\n258137 rrsw.dnb\ndir tmr\n215549 vbn\n$ cd qcq\n$ ls\ndir mmpgd\ndir tdsz\ndir tmfvsjwc\n$ cd mmpgd\n$ ls\n70793 jwbnpwnn\n$ cd ..\n$ cd tdsz\n$ ls\n246310 tdvrhhg.bzq\n$ cd ..\n$ cd tmfvsjwc\n$ ls\n103899 grpdmd.ggz\n287850 ldzslndn\n125930 llhr\n$ cd ..\n$ cd ..\n$ cd tmr\n$ ls\n83344 fbtfcg.hqp\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dqp\n$ ls\ndir lbgmcbv\ndir nbg\n$ cd lbgmcbv\n$ ls\n81776 wzdzzdp\n$ cd ..\n$ cd nbg\n$ ls\ndir mfsgjp\n155574 pflp.mdw\n$ cd mfsgjp\n$ ls\n199400 vdgcqdn.sqd\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd nfspb\n$ ls\n262412 csrdtbs\n73867 vbn\n136389 zqps.hjt\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd nhqwt\n$ ls\n123766 cvcl.jqh\ndir dhrtvctp\n222086 grpdmd.ggz\ndir gzg\n26005 lhpmz.tgz\ndir mcnjwwfr\n117122 msn.gst\n$ cd dhrtvctp\n$ ls\n224079 vdgcqdn.sqd\n$ cd ..\n$ cd gzg\n$ ls\n124395 dqp\ndir wqdbtqm\n$ cd wqdbtqm\n$ ls\n237354 pflp.mdw\n212019 vdgcqdn.sqd\n$ cd ..\n$ cd ..\n$ cd mcnjwwfr\n$ ls\n92504 cshdztf\ndir dctl\ndir dqp\ndir flcrmhlj\n161879 grpdmd.ggz\ndir gtt\ndir hlbnhchz\n220093 mdtdsgvm.zgg\ndir twntr\n287192 vbn\n$ cd dctl\n$ ls\ndir bbhch\n155396 hrrj.jzm\n164971 pblqmwj.vdb\ndir wnlgfpvf\n$ cd bbhch\n$ ls\ndir dpqtp\ndir jvdrcw\n$ cd dpqtp\n$ ls\n174135 gwb.qrb\n$ cd ..\n$ cd jvdrcw\n$ ls\n215993 dcqnblb.cqp\n200800 stjttf.ngc\n$ cd ..\n$ cd ..\n$ cd wnlgfpvf\n$ ls\n135978 cvcl.jqh\ndir dqp\n54018 lbrfmt\n$ cd dqp\n$ ls\n270516 dcqnblb.jqw\ndir dqp\n144626 grpdmd.ggz\n157731 hvcv.rhp\n133773 lnnt\n76250 vdgcqdn.sqd\n$ cd dqp\n$ ls\n41504 zprprf.cmc\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dqp\n$ ls\ndir dqp\ndir ldzslndn\n236737 mqzcvm.fjh\n239746 nhcdz.ncj\ndir rpchqq\n248824 vdgcqdn.sqd\n250937 zrchht.mwg\n$ cd dqp\n$ ls\n203381 qcq.djm\n$ cd ..\n$ cd ldzslndn\n$ ls\ndir dqp\ndir fptnzlv\ndir gmbnpm\ndir vhvblt\n$ cd dqp\n$ ls\n19579 qcq.lhg\n$ cd ..\n$ cd fptnzlv\n$ ls\n209930 dcqnblb\n$ cd ..\n$ cd gmbnpm\n$ ls\ndir ldzslndn\ndir qcq\n$ cd ldzslndn\n$ ls\n11075 pflp.mdw\n$ cd ..\n$ cd qcq\n$ ls\ndir tdp\n$ cd tdp\n$ ls\n40741 vdgcqdn.sqd\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd vhvblt\n$ ls\ndir lzr\n$ cd lzr\n$ ls\n62245 gbnj.llg\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd rpchqq\n$ ls\ndir bcs\ndir dcqnblb\ndir fvjzn\ndir lrphzrv\n$ cd bcs\n$ ls\n179794 bbn.dzb\n242069 cmjdmzjf.zgf\n1703 cvcl.jqh\ndir gnmhwj\ndir ldzslndn\n152520 qltpsz.jsj\ndir sqqjfps\n$ cd gnmhwj\n$ ls\ndir gvs\n201600 hptn.ftf\ndir hzrnb\ndir qcq\ndir sqhl\n$ cd gvs\n$ ls\n152358 zprprf.mlh\n$ cd ..\n$ cd hzrnb\n$ ls\n94290 gplsfd\n$ cd ..\n$ cd qcq\n$ ls\n91909 vmqd.bmg\n$ cd ..\n$ cd sqhl\n$ ls\n238673 vdgcqdn.sqd\n262885 zmdvr.nfg\n$ cd ..\n$ cd ..\n$ cd ldzslndn\n$ ls\n240461 mdz\n84303 qtj\n$ cd ..\n$ cd sqqjfps\n$ ls\n88753 fwn.tff\n$ cd ..\n$ cd ..\n$ cd dcqnblb\n$ ls\ndir dqp\n189996 dqp.pvp\n$ cd dqp\n$ ls\ndir qvfjz\n196506 vbn\n$ cd qvfjz\n$ ls\n209316 pflp.mdw\n107459 rwpbh.vpt\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd fvjzn\n$ ls\n241464 cvcl.jqh\ndir dqp\ndir ldzslndn\ndir msp\n125 pflp.mdw\n131895 vbn\n$ cd dqp\n$ ls\n34019 pflp.mdw\n202957 vbn\n$ cd ..\n$ cd ldzslndn\n$ ls\n147492 cvcl.jqh\n248719 spc.rfv\n$ cd ..\n$ cd msp\n$ ls\n184407 cvcl.jqh\n$ cd ..\n$ cd ..\n$ cd lrphzrv\n$ ls\ndir bbwqmbg\n81858 cvcl.jqh\ndir dqp\n248670 gqqsww.tsn\n199141 grpdmd.ggz\ndir ldzslndn\n34514 ldzslndn.ctw\ndir tln\n214615 zprprf.fwm\n$ cd bbwqmbg\n$ ls\n129750 flf\ndir pvlw\ndir qcq\n126 sqcqphz.tbm\n$ cd pvlw\n$ ls\n198005 jfvj.hdv\n$ cd ..\n$ cd qcq\n$ ls\ndir wgdzws\n$ cd wgdzws\n$ ls\n253522 ldzslndn.qwt\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd dqp\n$ ls\n281993 cvcl.jqh\ndir hwqjlwcb\n50532 msccz.qgm\n102187 trv.tnq\n111 wplnmj.bfl\n$ cd hwqjlwcb\n$ ls\n267580 dhjqb.dsb\n153195 ldzslndn.jqv\n41526 mvwcwc.zsc\n$ cd ..\n$ cd ..\n$ cd ldzslndn\n$ ls\n58666 cvcl.jqh\n79950 dqp.tmc\n242217 hns.lrb\ndir njswzh\n240692 vdgcqdn.sqd\ndir zvmjvcdm\n52909 zzh\n$ cd njswzh\n$ ls\n149732 cvcl.jqh\ndir rnmfd\n$ cd rnmfd\n$ ls\n75368 dqp.hmv\n14350 vbn\n$ cd ..\n$ cd ..\n$ cd zvmjvcdm\n$ ls\ndir jgczt\n$ cd jgczt\n$ ls\ndir qcq\n95941 qzvvwshv.jwc\n$ cd qcq\n$ ls\n273942 pflp.mdw\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd tln\n$ ls\ndir bmcng\n1518 lrg\ndir vnjfrhp\n$ cd bmcng\n$ ls\n38917 fqcrt\n$ cd ..\n$ cd vnjfrhp\n$ ls\ndir dcqnblb\ndir dqp\n247186 grpdmd.ggz\ndir ldzslndn\n169216 pflp.mdw\n206487 vdgcqdn.sqd\n16976 vlsrzjmb.mmc\n257938 wjl\n$ cd dcqnblb\n$ ls\ndir dqp\n$ cd dqp\n$ ls\n184133 qcq\n$ cd ..\n$ cd ..\n$ cd dqp\n$ ls\ndir dcqnblb\n31612 dqp.pnt\n212283 ldzslndn\n61600 vdbfc.ddj\n197189 wpv.wff\n$ cd dcqnblb\n$ ls\n62412 tfzllmrj\ndir zprprf\n$ cd zprprf\n$ ls\ndir bqnpsl\ndir dszrvpzc\n$ cd bqnpsl\n$ ls\n261548 spbsbbsw.cmn\n$ cd ..\n$ cd dszrvpzc\n$ ls\n188232 sggpqslr.smn\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ldzslndn\n$ ls\ndir bgnhd\ndir pgvcdzwz\ndir qgzhm\n$ cd bgnhd\n$ ls\n56989 cvcl.jqh\n$ cd ..\n$ cd pgvcdzwz\n$ ls\n110034 qhgnndv\n$ cd ..\n$ cd qgzhm\n$ ls\n247232 grpdmd.ggz\n269292 ldzslndn\n153843 tpz\ndir vnschqwr\n162392 wnq.btb\n$ cd vnschqwr\n$ ls\n43005 fvtvzfqm.jvc\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd flcrmhlj\n$ ls\n245668 dcqnblb.sdj\ndir lffj\n229909 pflp.mdw\n280176 vbn\n$ cd lffj\n$ ls\n116451 jmzz.jdd\ndir pjlwb\n162815 pmhlqq.snr\n226183 zffth\n$ cd pjlwb\n$ ls\n67518 qcq.hjq\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd gtt\n$ ls\n52105 grpdmd.ggz\n126869 zprprf.fgj\n$ cd ..\n$ cd hlbnhchz\n$ ls\n3064 dqp.lrw\n278756 grpdmd.ggz\n177208 ldzslndn.wlv\n141685 vbn\n$ cd ..\n$ cd twntr\n$ ls\n63747 cvcl.jqh\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd qcq\n$ ls\n226858 cwblp.zgp\ndir jjqsmfhr\ndir rjbqtrq\ndir vwmpnbts\n141715 wdbhdch\n286381 zprprf\n$ cd jjqsmfhr\n$ ls\ndir btmm\ndir fqndtlgq\n$ cd btmm\n$ ls\n4031 dqp.lrr\ndir fzdd\n$ cd fzdd\n$ ls\ndir vnwpn\n$ cd vnwpn\n$ ls\ndir bzlgsl\ndir ztvzrrbv\n$ cd bzlgsl\n$ ls\n9294 ldzslndn.sqr\n$ cd ..\n$ cd ztvzrrbv\n$ ls\n256017 cvcl.jqh\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd ..\n$ cd fqndtlgq\n$ ls\n271528 ccbmgp.bwd\n$ cd ..\n$ cd ..\n$ cd rjbqtrq\n$ ls\n122150 ldzslndn\n46467 tpdvp.pjf\n$ cd ..\n$ cd vwmpnbts\n$ ls\n47518 fcrwfzvm\n263343 gmc.lrt\n212764 qcq\n$ cd ..\n$ cd ..\n$ cd vwqwlqrt\n$ ls\ndir psrs\n$ cd psrs\n$ ls\n281998 zprprf.hml\n")

(deftest parse-command-test
  (is (= {:type :cd :arg "/"} (parse-command "$ cd /")))
  (is (= {:type :ls} (parse-command "$ ls")))
  (is (= {:type :dir :arg "gqcclj"} (parse-command "dir gqcclj")))
  (is (= {:type :file :name "c.dat" :size 8504156} (parse-command "8504156 c.dat"))))

(deftest update-path-test
  (is (= ["/"] (update-path ["/" "a" "b"] "/")))
  (is (= ["/" "a"] (update-path ["/" "a" "b"] "..")))
  (is (= '("/" "a") (update-path ["/" "a" "b"] "..")))
  (is (= ["/" "a" "b" "c"] (update-path ["/" "a" "b"] "c"))))

(deftest list-vector
  (is (= '(1 2 3) [1 2 3]))) ; wat!

(deftest add-file-test
  (is (= {:wd ["/"]
          :fs [{:path ["/"]
                :file {:name "readme.txt"
                       :size 10}}]}
         (add-file {:wd ["/"] :fs []}
                   {:name "readme.txt" :size 10}))))

(deftest size-contribution-test
  (is (= '({:dir "/" :size 29116} {:dir "/a" :size 29116})
         (size-contribution {:path ["/" "a"], :file {:name "f", :size 29116}}))))

(deftest solutions
  (is (= 95437 (day07-part1 example-input)))
  (is (= 1844187 (day07-part1 puzzle-input)))
  (is (= 24933642 (day07-part2 example-input)))
  (is (= 4978279 (day07-part2 puzzle-input))))

(run-tests)

(comment

  (= 95437 (day07-part1 example-input))
  (= 1844187 (day07-part1 puzzle-input))

  {:wd ["/" "d"],
   :fs [{:path ["/"], :file {:name "b.txt", :size 14848514}}
        {:path ["/"], :file {:name "c.dat", :size 8504156}}
        {:path ["/" "a"], :file {:name "f", :size 29116}}
        {:path ["/" "a"], :file {:name "g", :size 2557}}
        {:path ["/" "a"], :file {:name "h.lst", :size 62596}}
        {:path ["/" "a" "e"], :file {:name "i", :size 584}}
        {:path ["/" "d"], :file {:name "j", :size 4060174}}
        {:path ["/" "d"], :file {:name "d.log", :size 8033020}}
        {:path ["/" "d"], :file {:name "d.ext", :size 5626152}}
        {:path ["/" "d"], :file {:name "k", :size 7214296}}]}

  (def sample-fs [{:path ["/"], :file {:name "b.txt", :size 14848514}}
                  {:path ["/"], :file {:name "c.dat", :size 8504156}}
                  {:path ["/" "a"], :file {:name "f", :size 29116}}
                  {:path ["/" "a"], :file {:name "g", :size 2557}}
                  {:path ["/" "a"], :file {:name "h.lst", :size 62596}}
                  {:path ["/" "a" "e"], :file {:name "i", :size 584}}
                  {:path ["/" "d"], :file {:name "j", :size 4060174}}
                  {:path ["/" "d"], :file {:name "d.log", :size 8033020}}
                  {:path ["/" "d"], :file {:name "d.ext", :size 5626152}}
                  {:path ["/" "d"], :file {:name "k", :size 7214296}}]))
























