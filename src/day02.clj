(ns day02
  (:require
    [clojure.string :as str]))

;A - rock     - 1
;B - paper    - 2
;C - scissors - 3
;
;X - rock     - 1
;Y - paper    - 2
;Z - scissors - 3

(defn decrypt-play [play]
  (condp = play
    "A" :rock
    "B" :paper
    "C" :scissors
    "X" :rock
    "Y" :paper
    "Z" :scissors))

(defn play-value [play]
  (condp = play
    :rock 1
    :paper 2
    :scissors 3))

(defn winner-of [play]
  (condp = play
    :rock :paper
    :paper :scissors
    :scissors :rock))

(defn winner-score [p1 p2]
  (cond
    (= p2 p1) 3
    (= p2 (winner-of p1)) 6
    :else 0))

(defn score-of-play [play]
  (let [[p1 p2] (str/split play #" ")]
    (+
      (play-value (decrypt-play p2))
      (winner-score (decrypt-play p1) (decrypt-play p2)))))

(defn day02-part1 [input]
  (->> (str/split input #"\n")
       (map score-of-play)
       (apply +)))

; part 2
(def p1-encryption {"A" :rock
                    "B" :paper
                    "C" :scissors})

(def p2-encryption {"X" 0
                    "Y" 1
                    "Z" 2})

(def moves
  {:rock     [:scissors :rock :paper]
   :paper    [:rock :paper :scissors]
   :scissors [:paper :scissors :rock]})

(defn score-of-round [round]
  (let [[p1 p2] (str/split round #" ")
        m1 (get p1-encryption p1)
        outcome (get p2-encryption p2)
        m2 (get (get moves m1) outcome)]
    (+
      (* 3 outcome)
      (play-value m2))))

(defn day02-part2 [input]
  (->> (str/split input #"\n")
       (map score-of-round)
       (apply +)))

(def example-input "A Y\nB X\nC Z")
(def puzzle-input "A Z\nC X\nA Z\nA Z\nC Y\nC Y\nA Z\nA Y\nC Y\nA Y\nA Z\nA Z\nA Z\nA Y\nA Z\nA Y\nC Y\nC X\nA Y\nC Y\nC Y\nC X\nA Z\nC Y\nC X\nA X\nA Y\nA Z\nA Y\nA Y\nC X\nC X\nB Y\nC X\nC X\nA Y\nA Z\nA Z\nA X\nA Z\nA Z\nC Y\nA Z\nA Z\nA Y\nC X\nC Y\nC X\nB X\nC Z\nA Y\nA Z\nA Z\nA Z\nA Y\nA Y\nC X\nA Y\nA Z\nC Y\nA Y\nA Y\nA Z\nA Z\nC Y\nA Z\nC Y\nA Y\nA Z\nA Z\nC Y\nB Z\nA Z\nA Z\nA Z\nA Z\nC X\nC X\nA Y\nA X\nA Y\nA Z\nA Z\nC X\nA Z\nC X\nA Z\nC Y\nC X\nA Z\nA Z\nA Z\nA Z\nC X\nC Y\nA Z\nA Z\nC Y\nA Z\nB Z\nC X\nA Z\nA X\nC X\nA Z\nC Z\nA Z\nC X\nA Z\nA Y\nC X\nC Z\nA Z\nC X\nC Y\nA Z\nB Z\nB Y\nA X\nA X\nA X\nA Z\nA Z\nA X\nA Z\nA X\nA Z\nA Z\nC X\nC X\nB Z\nA Z\nA Y\nA Z\nA Z\nA Z\nA Z\nC X\nC X\nC X\nA Z\nA Z\nA Z\nA Z\nA X\nA Y\nA Y\nC X\nC X\nB Z\nC X\nA X\nA Z\nA Z\nC X\nC Z\nA Z\nA Z\nC Y\nA X\nA Z\nC Y\nA X\nA Y\nA Y\nA Y\nC Y\nA Z\nA Z\nC X\nC X\nC X\nC X\nB Z\nC Y\nC X\nC X\nA Y\nA Y\nA Z\nA Z\nA X\nC X\nA Z\nB Z\nA Z\nC X\nA Y\nA Z\nA Z\nA Y\nA Y\nA Z\nC X\nA Y\nC X\nC X\nA Z\nA Y\nC X\nA Z\nA Z\nA Y\nA Z\nA Z\nC Y\nC Z\nA Y\nA Z\nC X\nC X\nA Z\nA Z\nC Y\nA Y\nA Z\nA Y\nA Z\nC X\nA Z\nA Z\nC X\nA Y\nA Y\nC Y\nA Z\nC Y\nA Z\nA Z\nC Y\nA Y\nB Z\nC Y\nC X\nC X\nA Z\nC Y\nA X\nC Y\nA Z\nA Y\nA Z\nC X\nC X\nC Y\nC Y\nA Y\nA Z\nC X\nC X\nA Y\nA X\nA Y\nB Z\nA Y\nC Y\nC X\nC X\nA X\nC X\nB Z\nA X\nC Y\nC X\nC X\nA X\nA Z\nB Z\nA Z\nA Z\nA Y\nA X\nA Z\nC X\nA X\nC Y\nA Z\nA Z\nA X\nA Z\nA Z\nC X\nC X\nA Z\nA Z\nA Z\nA X\nA Z\nA X\nB Y\nA Z\nA Y\nC Y\nA Z\nC X\nA Z\nC X\nA Y\nA Z\nC X\nC Y\nA Y\nC Y\nA Z\nC X\nA Z\nA Z\nC X\nA Y\nA Z\nA X\nA Z\nA Z\nA Z\nB Y\nC X\nA X\nA Z\nA Z\nC Y\nC Y\nC X\nC X\nC X\nA X\nA X\nA Z\nA X\nA Y\nA Z\nA Y\nA Z\nC Y\nC Z\nA Y\nA Z\nA Z\nA X\nA Z\nA Z\nC X\nC Y\nA Y\nC X\nC X\nA Z\nC X\nC X\nC X\nA X\nA Z\nA Z\nA Z\nA Z\nB Y\nA Y\nA Y\nA Z\nC X\nA Y\nA Z\nC X\nA Z\nC Z\nA Y\nC X\nA Z\nB Z\nA Y\nA X\nA Z\nC X\nA Z\nA Z\nA Z\nA Z\nA Z\nB Z\nA Z\nC X\nA Y\nC X\nA Z\nA Z\nA Y\nA Z\nB Z\nC X\nA Y\nC Y\nA Z\nA Z\nC X\nC X\nA Y\nC X\nC Y\nB Z\nA Y\nC X\nA Y\nC X\nA X\nA Y\nA Z\nA Z\nA Y\nC X\nA X\nC X\nB Y\nA Z\nA Y\nB Y\nA Y\nC X\nA Z\nA Z\nC X\nC Y\nA Y\nC X\nC Y\nA Y\nA Z\nA X\nB Z\nC X\nA Z\nA Y\nA Z\nB Z\nA Z\nA X\nC Y\nA X\nA Z\nA Y\nC Y\nA Z\nC Y\nA Z\nC X\nC X\nA Y\nC X\nC X\nA Y\nA Z\nA Z\nA Y\nA X\nC Y\nA Z\nA Z\nC X\nA X\nA Z\nC Z\nA Z\nC Y\nA Z\nC X\nA Z\nA Z\nA X\nC X\nC X\nC Y\nB Z\nB Y\nC Y\nA Y\nA X\nA Z\nC X\nA Y\nA Y\nA Z\nA Z\nC Z\nC X\nC X\nC X\nA Z\nC X\nA Z\nA Z\nA Y\nC Y\nC X\nC X\nC X\nA Y\nC X\nB Z\nC X\nA Z\nC Y\nA Y\nC Y\nA Z\nA Z\nC X\nA X\nA Z\nA Y\nA Z\nB X\nC Z\nA Z\nC Z\nA X\nC X\nC X\nC X\nC X\nA Z\nA Y\nA Z\nA Z\nA Z\nA Z\nA Z\nC Z\nC Y\nC Y\nC X\nC Y\nA Z\nC X\nC X\nA Z\nA Z\nC X\nC Y\nC Y\nA Z\nA X\nC Y\nC Y\nC X\nA Y\nC X\nA Z\nA Z\nA Y\nC Y\nA Y\nC X\nC X\nA Z\nA Z\nC Y\nA Z\nC X\nA Y\nA Y\nA Z\nC Y\nA Z\nC X\nA Y\nA Z\nA Z\nA Z\nA Z\nC Y\nA Y\nC Y\nA Z\nA Z\nA X\nA Y\nA Y\nA Z\nC Y\nA Z\nA X\nA Z\nB Z\nC X\nC X\nC Y\nA Z\nA Z\nC X\nC Z\nA Z\nC X\nC Y\nA Z\nB Z\nA Z\nB Z\nA X\nA Y\nA Z\nA Z\nA Z\nA Z\nC Y\nA Z\nA Z\nA Z\nB Z\nA Z\nC X\nC X\nA Z\nC X\nA X\nA Z\nA Y\nA Y\nA X\nA Z\nA Z\nA Z\nA Z\nB Z\nA Z\nC Y\nC Y\nC X\nC X\nB Z\nC Y\nA Z\nC Y\nA Z\nA Y\nC Y\nA X\nA Y\nC X\nA Y\nC X\nA Z\nA Z\nB Z\nA Y\nC Y\nC X\nA Z\nA Z\nC X\nA Y\nA Z\nA Z\nA X\nA Y\nA Y\nA Y\nA Z\nA Y\nA Z\nC X\nA X\nA Z\nA Z\nC Y\nA Z\nC X\nA Z\nC Y\nA Y\nA Z\nA Z\nA Z\nA Z\nC Y\nA X\nA Z\nA Z\nA Y\nA Z\nB Z\nA X\nA Y\nC X\nC X\nA Y\nA Z\nC Y\nA Z\nA Y\nA Y\nA Z\nA Y\nA Y\nA Z\nA Z\nA Z\nC X\nA Z\nA Y\nA Z\nA Z\nC Y\nA Z\nC X\nA Y\nC X\nC Y\nA Z\nC Y\nA Z\nA Y\nC Y\nC Y\nA Z\nC X\nC X\nC X\nA Z\nA Z\nA Y\nC X\nA Z\nA X\nA Z\nA Z\nC X\nA Z\nC Y\nA Y\nA Z\nA Y\nA Z\nA Z\nC Y\nA Z\nC X\nA Z\nA X\nA Z\nA Z\nC X\nA Z\nA Z\nC Y\nC X\nA Z\nC Y\nC X\nC X\nA Z\nA Z\nA Z\nA X\nC X\nA Z\nA Z\nC Y\nA Y\nC X\nA Y\nC X\nC Y\nA Y\nA Z\nC X\nA Z\nB Y\nA X\nB Z\nA Y\nA X\nC Y\nA Z\nA X\nA Z\nA Z\nC Y\nA Z\nB Z\nC X\nC X\nB Y\nA Z\nA Z\nA Z\nC Y\nC X\nA Z\nA Y\nA Y\nA Y\nC Y\nC X\nA Z\nA X\nA X\nA X\nA Z\nA Z\nA Z\nA Z\nC X\nC X\nA Z\nC X\nA Y\nC X\nB Z\nA Z\nA Z\nC X\nA Z\nC X\nC X\nC X\nA Y\nC Y\nC X\nA Y\nC X\nC Y\nB Z\nA Z\nC X\nA Z\nA X\nB Z\nA Y\nB Y\nA Z\nA Z\nA X\nA Z\nA X\nA Z\nA Z\nC X\nC Y\nA X\nC Y\nC X\nA Z\nA Z\nA Z\nC Y\nA X\nA Y\nA Z\nA Y\nC X\nB Z\nA Z\nA Y\nC Z\nC X\nA Z\nA Z\nA Z\nB Z\nA X\nC X\nA Z\nA Z\nB Z\nA Z\nA Z\nB Z\nA Z\nC X\nA Z\nC X\nA Z\nC X\nA Z\nC Y\nA Z\nA X\nA Y\nA Y\nC X\nA Y\nC X\nB Z\nA Z\nA Z\nA Z\nA Z\nC X\nA Z\nC X\nA Z\nA Y\nA Z\nB Z\nA Y\nC Z\nA Y\nC X\nA Z\nA Z\nA Y\nB Z\nA X\nC Y\nA Z\nA Y\nA Z\nA Y\nA Y\nA Z\nA Z\nA Z\nA X\nA Z\nC Y\nA Y\nA X\nA Y\nC X\nA Y\nC X\nA X\nC X\nA Z\nC Y\nA Y\nA Z\nA Z\nA X\nA Y\nC X\nC X\nA Y\nA X\nA Z\nB Z\nA Y\nA Z\nA X\nA Z\nB Y\nA Y\nA Y\nA Z\nC X\nA Z\nA Z\nA Y\nA Z\nC Z\nA Z\nA Z\nA Z\nA Z\nC Y\nC Y\nA Y\nB Z\nC Y\nA Y\nC Y\nA Z\nA Z\nA Z\nA Z\nC Y\nA Z\nA Z\nC Y\nC X\nA Y\nA Y\nA Z\nC X\nC Z\nC X\nC X\nA Z\nA Z\nA Y\nA Z\nA X\nC Y\nA Z\nA Z\nC Y\nC X\nA X\nA Z\nA Z\nA X\nC X\nC X\nC X\nA Z\nA X\nC X\nC Y\nA X\nA Z\nC X\nA Z\nA Z\nC X\nA Y\nA Z\nA Z\nA Y\nA Z\nC X\nA Z\nA Z\nC X\nA Y\nA Z\nA Z\nC X\nA Z\nA Y\nA Z\nC Y\nA X\nA Z\nA Z\nC X\nA Z\nA Y\nC Y\nB Z\nA Z\nA Y\nC X\nA Z\nB Z\nA Z\nC Z\nA X\nA Z\nA Z\nC Y\nA Z\nA Y\nC X\nC Y\nA Z\nA Z\nA Y\nA X\nC Y\nA Y\nC X\nC Y\nA Z\nC Y\nA Z\nC X\nA Z\nA Z\nA X\nA Z\nA Z\nB X\nA X\nA Z\nC Y\nA Z\nA X\nC X\nA Z\nA Z\nC X\nC Y\nC Y\nA X\nA Y\nC Y\nA Y\nA Z\nA Z\nA Z\nA Z\nA Y\nC X\nC X\nC Y\nC X\nA Z\nA X\nB Z\nB Y\nC X\nC Y\nA Y\nA Z\nA Y\nC X\nC Z\nA Z\nA Y\nC Y\nC X\nA Z\nA Z\nA Y\nC X\nC Z\nC Y\nA Z\nC X\nC Y\nA X\nA X\nA Y\nA Z\nB Z\nA X\nA Y\nA Y\nC X\nC Y\nA Z\nA X\nA Z\nA X\nA Y\nA Z\nA Z\nA Z\nC X\nA Z\nA Z\nA Z\nC Z\nC Y\nC Y\nA Z\nC Y\nC Y\nC Y\nC X\nA Z\nC X\nC X\nA Z\nA Y\nA Z\nA Z\nA X\nA Y\nA Y\nC X\nC X\nA Z\nA Z\nA Z\nA Z\nA Y\nA Z\nA Z\nA Z\nA Z\nA X\nA X\nA Y\nA X\nC Y\nA Y\nA Z\nC X\nA Y\nA Y\nA Z\nA Z\nA Z\nC X\nA Z\nC X\nC X\nC Y\nA Y\nA Z\nA Y\nA Z\nC X\nC X\nA Z\nA Z\nA Z\nC X\nA X\nA Z\nA Z\nC Y\nC Y\nA Y\nA Y\nA Z\nA Z\nC Y\nC X\nC Y\nA X\nC Y\nC X\nC Y\nA Z\nA Z\nA X\nC Y\nC Y\nA Z\nA Y\nC X\nA X\nB Z\nA Z\nC X\nA Y\nA X\nA Z\nA Z\nA Z\nC Y\nC X\nA Z\nC X\nA Z\nA Z\nA Y\nA Z\nA Y\nA X\nA Z\nC Z\nA Z\nA Z\nA Z\nA X\nA Z\nA X\nC X\nA Z\nA Z\nC Y\nA X\nA Z\nC X\nC X\nA Y\nA Z\nA Z\nC X\nB X\nA Z\nC Y\nA Z\nC X\nA Y\nA Z\nC X\nA X\nA Z\nA X\nA Z\nC X\nA Z\nA X\nA Z\nC X\nC Y\nA Y\nA X\nA X\nA Z\nC Y\nC Y\nA Y\nA Y\nA Y\nA X\nA Z\nA Z\nA Z\nC Y\nC Y\nA Y\nB X\nB X\nA Z\nC X\nC X\nA Z\nC Y\nC X\nA Z\nA Z\nA Z\nA Z\nC X\nA Z\nA Z\nC Y\nA Y\nC Y\nA Z\nC Y\nC Y\nA Z\nC X\nA Z\nA X\nA Z\nC X\nC X\nA Y\nB Z\nA Y\nC Y\nA Z\nC Y\nA X\nC X\nA Y\nC X\nA Z\nC Z\nC Y\nA Z\nC X\nC Y\nA Z\nA X\nA Z\nA Z\nA Z\nC X\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Y\nA Z\nA X\nA Y\nC Y\nB Y\nC X\nB Z\nA Z\nA Z\nA Y\nB Z\nA Z\nA Z\nC X\nC Y\nC X\nA Y\nA X\nC X\nC Y\nA Y\nC X\nC Y\nA Z\nA Z\nA Z\nA Y\nC X\nA Y\nC X\nB Z\nA Z\nA Y\nA Z\nA Z\nA Y\nC X\nA Z\nC X\nC Y\nA Y\nA Z\nB Z\nC X\nA Y\nC X\nA Y\nA Z\nC X\nA X\nC X\nB Y\nC X\nA Z\nA Y\nA Z\nA Y\nA X\nC X\nC X\nA Y\nC X\nA Y\nA Y\nA X\nB Y\nA Y\nC X\nC X\nA Y\nB Z\nB X\nB Z\nA Y\nA Z\nC Y\nA Y\nB Y\nA Z\nC X\nA Z\nA Z\nA Z\nA Z\nB Z\nC X\nC Y\nA Z\nC Y\nC Y\nA X\nC X\nA Z\nA Z\nA Z\nC X\nC X\nC X\nA Y\nC Z\nC Z\nA Z\nC X\nA Y\nA Z\nA Z\nA Z\nC X\nA Z\nA X\nA Z\nA Z\nA Z\nA Z\nA Y\nC Y\nC X\nA X\nA Y\nC X\nA X\nA Z\nA Z\nC X\nA Z\nA X\nA Z\nA Z\nA X\nA Z\nA Z\nA X\nA Z\nA X\nB Y\nA Y\nA Y\nC Y\nA Z\nA Y\nC X\nA Z\nA Y\nA X\nC Y\nB X\nC Y\nA Z\nC X\nA Y\nA Z\nA Y\nA X\nC Y\nA Z\nA Z\nC Y\nC X\nA Z\nC X\nA Y\nC X\nA Z\nA Y\nA Z\nA Z\nA Z\nA Y\nA Z\nC X\nC X\nA X\nC X\nC X\nA Z\nC X\nA Z\nC Y\nC X\nA Z\nA Z\nA Z\nC X\nA X\nC Y\nA Z\nC Y\nA X\nA Z\nC X\nA Z\nA Z\nA X\nA Z\nC X\nB X\nA Z\nA Z\nA Z\nC X\nA Y\nA Y\nA X\nC Y\nC Y\nA Z\nA Y\nA Z\nA Z\nC X\nA X\nA Y\nA Z\nA Z\nA Z\nA Z\nB Z\nC X\nC X\nC X\nA Z\nC Z\nA X\nC X\nA Z\nC Y\nA Z\nA Z\nA Y\nA Y\nC X\nA Z\nA X\nA Z\nA Z\nA Z\nA Z\nC Y\nA Z\nA Y\nA X\nA X\nA Z\nC X\nA X\nA X\nA Z\nA Y\nC X\nA Z\nA Z\nA Y\nA Z\nB Z\nC X\nC X\nC Z\nC Y\nC X\nA Z\nC Y\nA Z\nC Z\nA Z\nA Y\nA Y\nA X\nA X\nA Z\nA Y\nA Y\nA Y\nA Y\nA Z\nC Y\nA Z\nA Z\nC X\nA Z\nA Z\nC Y\nA Y\nC X\nA Y\nC X\nA Z\nB Z\nA X\nB X\nA Y\nA X\nA Y\nB Z\nA Y\nA Z\nC Y\nC Y\nA Z\nA X\nA Z\nA Z\nC Z\nA Z\nA Y\nC X\nA Y\nC X\nA X\nA Y\nC Y\nA Y\nA Z\nA Z\nC X\nC X\nB Z\nA Z\nA Z\nA X\nC X\nC Y\nA Z\nA Z\nA X\nC X\nC Z\nA Z\nC Y\nA Y\nB Z\nC Y\nA Z\nC X\nA X\nA Z\nA Z\nA Z\nA Y\nC Y\nA Z\nC Y\nA Z\nA Z\nA Z\nA Z\nA Y\nC X\nA Y\nC Y\nB Z\nA Z\nC X\nC Y\nA Z\nC X\nA Z\nC X\nC Z\nA Z\nC X\nC X\nA Z\nA Z\nA Y\nA Y\nA Y\nC X\nA Y\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Y\nA Z\nA X\nC Y\nA Z\nA Z\nA Z\nA Y\nA Z\nA Z\nA Z\nA Z\nC X\nB Z\nA Z\nA Y\nA Y\nA X\nA Z\nA Z\nC Z\nA Z\nC X\nA Y\nA X\nB Z\nA Z\nA Z\nA Z\nC Y\nC Z\nC X\nA Z\nC Y\nC Y\nC Y\nC X\nB Z\nA Z\nA Z\nC Z\nA X\nA Z\nA Z\nA Z\nC X\nA Z\nA Z\nC Y\nC Y\nA Z\nA Z\nC X\nA Y\nC Y\nC Y\nA Z\nA Z\nA X\nA Z\nA Z\nA X\nA X\nC X\nA Z\nA X\nC X\nC X\nA Z\nA X\nA Z\nC X\nC X\nC X\nC Y\nA Z\nA Z\nA X\nA Z\nA Y\nA Z\nA Z\nC Y\nA Z\nA Z\nA Z\nA X\nA Z\nC X\nA X\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Y\nA X\nA Y\nA X\nC Y\nA Z\nA Z\nC X\nA Z\nA Z\nA Z\nA X\nA Z\nA Z\nA Y\nC X\nA Y\nA Z\nC X\nA X\nA Y\nA Z\nA Z\nA X\nA Z\nA Y\nC X\nA Y\nA Z\nA Z\nA Z\nA Z\nC X\nC Y\nA Z\nB Z\nC X\nA Z\nA Z\nC Y\nC Y\nC X\nA X\nC Y\nB Y\nA Z\nA Z\nA Z\nC Y\nA Z\nA X\nA Y\nA Z\nA Z\nA Z\nC Y\nA Z\nC Y\nC X\nA Z\nA Z\nA Z\nA Z\nA Z\nA X\nA Z\nA Z\nC X\nA Z\nA Y\nC Z\nA Z\nA Z\nA Z\nB X\nC X\nA Z\nA Z\nA Z\nA Z\nC X\nA Z\nA Z\nA Y\nA X\nC X\nC Y\nA X\nA Y\nC X\nA Z\nA Z\nC X\nC X\nA Z\nC X\nA Z\nA Y\nC X\nA Z\nB X\nB Y\nA X\nC Y\nA X\nA Y\nC Y\nA Z\nA Z\nA Z\nC X\nA Z\nA X\nA Z\nC Y\nA Z\nA Z\nC X\nA Z\nA Z\nA Y\nA Z\nA X\nA Y\nA Z\nC X\nC Y\nB X\nC Y\nA Y\nA Z\nC X\nA Z\nC X\nC Y\nA X\nA X\nA Z\nA Z\nC Y\nA Y\nA Y\nA Z\nA Z\nC X\nA X\nC X\nA X\nA Y\nC X\nA Z\nA Z\nB Y\nA Z\nA Z\nA Z\nA Z\nA Z\nA X\nA Y\nA X\nA Z\nA Z\nC X\nA Y\nA Z\nC Y\nC X\nC X\nC X\nC X\nC Y\nA X\nA X\nC Y\nA X\nA Y\nA Y\nB Z\nA Z\nB Y\nC Y\nA X\nA Y\nA Z\nA Z\nA Z\nA X\nC X\nA Z\nA Z\nA Z\nA Z\nA Z\nA Y\nA Z\nB Z\nA Z\nA Z\nA Y\nC Y\nC Y\nC X\nA Z\nA X\nC X\nA Y\nB Z\nC X\nA Z\nC X\nC Y\nC Y\nA X\nA X\nC X\nA Z\nA Z\nA Y\nA X\nA X\nA Z\nC Y\nB X\nA Z\nA Z\nA Z\nA Z\nA Y\nA Y\nA Z\nC Y\nC Y\nA X\nA Z\nA Z\nC Y\nA Y\nA Y\nA X\nA Y\nA X\nA X\nA X\nA Z\nA Z\nA Y\nA Z\nC X\nA Z\nA Y\nA Z\nA Z\nA Y\nA Z\nA Z\nC X\nA Z\nA Y\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Y\nC X\nB Z\nC Z\nA Z\nA Y\nA X\nA X\nC Y\nC Y\nA X\nC X\nB Z\nA Z\nC X\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Y\nA Z\nA Z\nA Y\nA Z\nA Y\nA Y\nA Z\nA Z\nA Y\nA Z\nC Y\nA Z\nA Z\nA Z\nC X\nC X\nA Z\nC Y\nA Z\nA Z\nC X\nA Z\nC X\nA Y\nA Z\nC Y\nA Z\nA Z\nC X\nC Y\nA Z\nC X\nC X\nA X\nA Y\nC Y\nA Y\nA Z\nC X\nA Z\nA Z\nA X\nA Z\nC Y\nA Z\nC X\nA Z\nC X\nA Z\nC X\nA Y\nC Y\nA X\nA Z\nA Z\nC Y\nA X\nA Z\nA Y\nB Z\nA Z\nA X\nA Z\nA Z\nA X\nC X\nA Z\nA Z\nC Y\nA Z\nA Y\nA Z\nC Y\nA Z\nA Y\nA Z\nC Z\nA Y\nA Z\nA Y\nC Y\nA Z\nC X\nA X\nB Z\nC X\nC X\nA Z\nA Z\nA Y\nA X\nA Z\nA Z\nB Z\nA Z\nC Z\nA X\nA Z\nA Z\nA Z\nA Z\nA X\nA X\nA Z\nA X\nA Z\nA Z\nA Z\nA X\nC X\nC Y\nA X\nC X\nC X\nA Z\nA X\nC Y\nB Z\nA Z\nA Z\nC X\nB X\nA Z\nA Z\nC X\nC X\nB Z\nC Y\nA Z\nA Z\nC X\nA X\nA Y\nC Y\nC Y\nB Z\nA Y\nC X\nA Z\nA Z\nA Y\nC X\nA Z\nA Z\nC X\nC Y\nA Z\nC X\nC Y\nC Y\nC X\nC Y\nA Z\nC Y\nA Z\nC X\nA Z\nC Y\nC Z\nA Z\nB Z\nA Z\nC X\nC X\nB Y\nB Z\nC Y\nC X\nC X\nA Y\nC X\nC X\nA Z\nA Y\nA Z\nC X\nA X\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nC X\nC X\nA Z\nC Y\nC X\nA Z\nB Z\nA Z\nA Z\nA X\nC X\nA Y\nA Z\nA Z\nA Y\nA X\nC Y\nB X\nA Z\nA X\nC Y\nC X\nC X\nC Y\nC Y\nA Z\nA Z\nC Y\nA Z\nA Y\nC Y\nA Y\nC X\nA Z\nC X\nC Y\nC Y\nA Z\nA Z\nA X\nA Y\nA Z\nA X\nA Z\nA Z\nA Z\nC Y\nA Z\nA Z\nC X\nC Y\nA Z\nA Z\nA Z\nC Y\nC X\nA Z\nC X\nA Z\nA Z\nA Z\nA X\nA Z\nA Z\nA Y\nB Z\nA Z\nA Z\nA Z\nC Y\nA Z\nB Z\nA Z\nC Z\nA Z\nA Y\nC X\nC Y\nC X\nC X\nA Z\nA Z\nA Y\nA Y\nA Z\nA Y\nB Z\nC Y\nA Y\nA Z\nC X\nA Z\nA Z\nC Y\nA Y\nA Z\nA Y\nC Y\nA Z\nA Z\nA X\nB Z\nA Z\nA X\nC X\nA Z\nC Y\nC Y\nA Z\nB Y\nA Y\nA Z\nA Z\nA Z\nA Z\nC X\nC X\nA Z\nC X\nA Y\nA Z\nA Z\nC Y\nA Z\nA Z\nC Y\nC X\nB Z\nA Y\nA Y\nC X\nC X\nA Z\nA X\nB Z\nA Z\nC Y\nA Z\nA Y\nA Z\nA Z\nA Y\nC Y\nC X\nA Z\nC X\nA Z\nC Y\nA Z\nC Y\nA Y\nA Z\nA Y\nA Z\nC X\nA Z\nA Z\nC X\nA Z\nB Y\nA Z\nA Z\nC Y\nC X\nC X\nA Z\nA Z\nC X\nB Z\nA Y\nA Z\nA Y\nA Z\nA Z\nA Y\nA X\nC X\nC X\nA X\nA Z\nA Y\nA Y\nA Z\nA Y\nA Z\nA Z\nC Y\nA X\nA Z\nA Z\nC X\nA Z\nA X\nB X\nC X\nA Z\nA Y\nB Z\nC X\nC Y\nA Z\nB Z\nC Y\nA Z\nA Z\nA X\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\nA Z\n")

(comment
  (= :rock (decrypt-play "A"))

  (= 1 (play-value :rock))
  (= 2 (play-value :paper))
  (= 3 (play-value :scissors))

  (str/split "A X" #" ")
  (day02 example-input)
  (let [[p1 p2] (str/split "A X" #" ")]
    (print p1)
    (print p2))

  (winner-of :rock)

  (= 15 (day02-part1 example-input))
  (= 11767 (day02-part1 puzzle-input))

  (= 12 (day02-part2 example-input))
  (= 13886 (day02-part2 puzzle-input))


  (get p1-encryption "A")
  (get p2-encryption "Y"))

















